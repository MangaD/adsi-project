# ADSI Project

**Course:** Data Administration in Information Systems  
**University:** Instituto Superior Técnico  
**Academic year:** 2019-20

### Team

- 73891 — David Gonçalves  ([david.s.goncalves@tecnico.ulisboa.pt](mailto:david.s.goncalves@tecnico.ulisboa.pt))
- 94112 — Gonçalo Pires ([goncalo.estevinho.pires@tecnico.ulisboa.pt](mailto:goncalo.estevinho.pires@tecnico.ulisboa.pt))

### Introduction:

The goal of this project is to answer some exercises related to database administration in a relational database schema. It covers SQL Server storage and indexing; B+ Trees; extendable hashing; query processing and optimisation; transactions, concurrency control and recovery management; and database tuning.

### Assignment:

- [DB2_4_BplusTreeExample.pdf](DB2_4_BplusTreeExample.pdf).

### Report:
- [T6_Fri_0800_0-14_project.pdf](T6_Fri_0800_0-14_project.pdf)